﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Reflection;

using ReSys.Model;
using Microsoft.VisualBasic;

namespace ReSys
{
    static class Database
    {
        public static string[] ExceptionTable { get; } = {
            "ID", "Distance", "EndDate", "Airplane", "Tickets"
        };

        private static readonly string[] ValidKeys =
        {
            "Airports", "Routes", "Airplanes", "Flights", "Tickets", "IndividualClients", "CompanyClients"
        };

        private static List<IModel> models;

        public static IReadOnlyList<IModel> Models => models;

        public static IReadOnlyList<Airport> Airports => Models.OfType<Airport>().ToList();

        public static IReadOnlyList<IndividualClient> IndividualClients => Models.OfType<IndividualClient>().ToList();

        public static IReadOnlyList<CompanyClient> CompanyClients => Models.OfType<CompanyClient>().ToList();

        public static IReadOnlyList<Route> Routes => Models.OfType<Route>().ToList();

        public static IReadOnlyList<Airplane> Airplanes => Models.OfType<Airplane>().ToList();

        public static IReadOnlyList<Flight> Flights => Models.OfType<Flight>().ToList();

        public static IReadOnlyList<Ticket> Tickets => Models.OfType<Ticket>().ToList();

        public static IReadOnlyList<Client> Clients
        {
            get
            {
                List<Client> clients = new List<Client>();
                clients.AddRange(IndividualClients);
                clients.AddRange(CompanyClients);
                clients = clients.OrderBy(x => x.ID).ToList();
                return clients;
            }
        }

        public static Dictionary<string, string> Lists { get; private set; }

        public static IReadOnlyList<IModel> GetListByType(Type type)
        {
            string typeName = type.Name;
            return typeName switch
            {
                "Airport" => Airports,
                "IndividualClient" => Clients,
                "CompanyClient" => Clients,
                "Client" => Clients,
                "Route" => Routes,
                "Airplane" => Airplanes,
                "Flight" => Flights,
                "Ticket" => Tickets,
                _ => null,
            };
        }

        public static IReadOnlyList<IModel> GetListByTypeName(string typeName)
        {
            return typeName switch
            {
                "Airport" => Airports,
                "IndividualClient" => Clients,
                "CompanyClient" => Clients,
                "Client" => Clients,
                "Route" => Routes,
                "Airplane" => Airplanes,
                "Flight" => Flights,
                "Ticket" => Tickets,
                _ => null,
            };
        }

        public static void Init()
        {
            Lists = new Dictionary<string, string>();
            models = new List<IModel>();
        }

        public static void Add(IModel item)
        {
            if (CheckForDuplicate(item))
                return;
            IReadOnlyList<IModel> list = GetListByType(item.GetType());
            if (list.Count != 0)
                item.ID = list[^1].ID + 1;
            else
                item.ID = 1;
            models.Add(item);
        }

        public static void Remove(IModel item)
        {
            // For types than need additional "care".
            // For most types setting dependencies to null (bottom loop) is enough.
            IList toRemove = new List<IModel>();
            switch (item.GetType().Name)
            {
                case "Airport":
                    toRemove = Routes.ToList().FindAll(x => x.Destination == item || x.Source == item);                    
                    break;
                case "Route":
                case "Airplane":
                    toRemove = Flights.ToList().FindAll(x => x.Airplane == item || x.Route == item);                    
                    break;
                case "Client":
                case "IndividualClient":
                case "CompanyClient":
                case "Flight":
                    toRemove = Tickets.ToList().FindAll(x => x.Client == item || x.Flight == item);
                    break;
            }

            if(toRemove.Count != 0)
                foreach (IModel i in toRemove)
                    Remove(i);

            foreach (IModel model in models)
            {
                foreach (var prop in model.GetType().GetProperties())
                {
                    if (prop.GetValue(model) == item)
                        prop.SetValue(model, null);
                }
            }

            models.Remove(item);
        }

        public static void Edit(IModel original, IModel modified)
        {
            foreach (var prop in original.GetType().GetProperties())
            {
                if (prop.Name != "ID")
                    prop.SetValue(original, prop.GetValue(modified));
            }
        }

        public static void ReadFromFile(string path)
        {
            if (!File.Exists(path))
            {
                StreamWriter writer = File.CreateText(path);
                writer.WriteLine("airports: airports.txt");
                writer.WriteLine("routes: routes.txt");
                writer.WriteLine("airplanes: airplanes.txt");
                writer.WriteLine("flights: flights.txt");
                writer.WriteLine("tickets: tickets.txt");
                writer.WriteLine("individualClients: individual_clients.txt");
                writer.WriteLine("companyClients: company_clients.txt");
                writer.Close();
            }
            StreamReader master = File.OpenText(path);
            string line;
            string[] split;
            while ((line = master.ReadLine()) != null)
            {
                split = line.Split(' ');
                split[0] = (char.ToUpper(split[0].First()) + split[0].Substring(1))[0..^1];
                if (ValidKeys.Contains(split[0]))
                {
                    Lists[split[0]] = split[1];
                }
            }

            foreach (var key in Lists.Keys)
            {
                if (!File.Exists(Lists[key]))
                    continue;
                StreamReader sr = File.OpenText(Lists[key]);
                IList list = typeof(Database).GetProperty(key).GetValue(null) as IList;
                Type type = list.GetType().GetProperty("Item").PropertyType;
                while ((line = sr.ReadLine()) != null)
                {
                    object obj = null;
                    split = line.Split(";");
                    obj = Activator.CreateInstance(type, new Object[] { split });
                    models.Add((IModel)obj);
                }
                sr.Close();
            }

        }

        public static void WriteToFile()
        {
            foreach (var key in Lists.Keys)
            {
                StreamWriter sw = File.CreateText(Lists[key]);
                IEnumerable<IModel> fields = typeof(Database).GetProperty(key).GetValue(null) as IEnumerable<IModel>;
                foreach (var curr in fields)
                {
                    sw.WriteLine(curr.ToDatabaseFormat());
                }
                sw.Close();
            }
        }

        private static bool CheckForDuplicate(IModel item)
        {
            bool duplicate = false;
            var list = GetListByType(item.GetType());
            foreach (var model in list)
            {
                if (model.GetType() != item.GetType())
                    continue;
                bool allEqual = true;
                foreach (var prop in item.GetType().GetProperties())
                {
                    if (prop.Name == "ID")
                        continue;
                    if (!prop.GetValue(model).Equals(prop.GetValue(item)))
                    {
                        allEqual = false;
                        break;
                    }
                }
                if (allEqual)
                {
                    duplicate = true;
                    break;
                }
            }

            return duplicate;
        }

    }
}
