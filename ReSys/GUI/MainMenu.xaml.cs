﻿using ReSys.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReSys
{    
    public partial class MainMenu : UserControl
    {
        public MainMenu()
        {
            InitializeComponent();
            generateFlight.Click += GenerateFlight_Click;
            reserveTicket.Click += ReserveTicket_Click;
            revokeTicket.Click += RevokeTicket_Click;
        }

        private void RevokeTicket_Click(object sender, RoutedEventArgs e)
        {
            TicketRevokeForm form = new TicketRevokeForm();
            form.Init();
        }

        private void ReserveTicket_Click(object sender, RoutedEventArgs e)
        {
            TicketReservationForm form = new TicketReservationForm();
            form.Init();
        }

        private void GenerateFlight_Click(object sender, RoutedEventArgs e)
        {
            FlightGenerator generator = new FlightGenerator();
            generator.Init();
        }
    }
}
