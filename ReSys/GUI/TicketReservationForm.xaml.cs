﻿using ReSys.Model;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReSys
{
    public partial class TicketReservationForm : UserControl
    {
        private Window window;
        public TicketReservationForm()
        {
            InitializeComponent();
            clients.ItemsSource = Database.Clients;

            clients.SelectionChanged += Clients_SelectionChanged;
            flights.SelectionChanged += Flights_SelectionChanged;
            buttonReserve.Click += ButtonReserve_Click;
        }

        private void ButtonReserve_Click(object sender, RoutedEventArgs e)
        {
            Database.Add(new Ticket(flights.SelectedItem as Flight, clients.SelectedItem as Client));
            window.Close();
        }

        private void Flights_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (flights.SelectedIndex == -1)
                buttonReserve.IsEnabled = false;
            else
                buttonReserve.IsEnabled = true;
        }

        private void Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (clients.SelectedIndex != -1)
            {
                List<Flight> validFlights = new List<Flight>();
                foreach (Flight flight in Database.Flights)
                {
                    List<Ticket> tickets = Database.Tickets.ToImmutableList().FindAll(x => x.Flight == flight).ToList();
                    if (!(Database.Tickets.Any(x => x.Flight == flight && x.Client == clients.SelectedItem as Client)
                                       || tickets.Count >= flight.Airplane.Capacity
                                       || flight.StartDate < DateTime.Now ))
                    {
                        validFlights.Add(flight);
                    }
                }
                flights.ItemsSource = validFlights;
            }
        }

        public void Init()
        {
            window = new Window()
            {
                Width = 300,
                Height = 350,
                Title = "Generate flight",
                ResizeMode = ResizeMode.NoResize,
            };
            window.Content = this;
            window.ShowDialog();
        }
    }
}
