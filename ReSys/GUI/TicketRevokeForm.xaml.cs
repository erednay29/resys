﻿using ReSys.Model;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReSys
{
    public partial class TicketRevokeForm : UserControl
    {
        private Window window;

        public TicketRevokeForm()
        {
            InitializeComponent();
            clients.ItemsSource = Database.Clients;

            clients.SelectionChanged += Clients_SelectionChanged;
            tickets.SelectionChanged += Tickets_SelectionChanged;
            buttonRevoke.Click += ButtonRevoke_Click;
        }

        private void Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(clients.SelectedIndex != -1)
            {
                List<Ticket> clientTickets = Database.Tickets.ToImmutableList()
                    .FindAll(x => x.Client == clients.SelectedItem as Client
                    && x.Flight.EndDate > DateTime.Now)
                    .ToList();
                tickets.ItemsSource = clientTickets;
            }
        }

        private void Tickets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tickets.SelectedIndex == -1)
                buttonRevoke.IsEnabled = false;
            else
                buttonRevoke.IsEnabled = true;
        }

        private void ButtonRevoke_Click(object sender, RoutedEventArgs e)
        {
            Database.Remove(tickets.SelectedItem as Ticket);
            window.Close();
        }

        public void Init()
        {
            window = new Window()
            {
                Width = 300,
                Height = 350,
                Title = "Generate flight",
                ResizeMode = ResizeMode.NoResize,
            };
            window.Content = this;
            window.ShowDialog();
        }
    }
}
