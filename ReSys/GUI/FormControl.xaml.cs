﻿using ReSys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace ReSys
{
    public enum FormMode
    {
        Edit, Add
    }

    public partial class FormControl : UserControl
    {
        private IModel original;
        private Type fieldType;
        private Window window;
        private readonly FormMode formMode;

        public FormControl(FormMode mode)
        {
            InitializeComponent();
            submit.Click += Submit_Click;
            this.formMode = mode;
        }

        private bool Validate()
        {
            ItemCollection items = itemsControl.Items;
            for (int i = 1; i < items.Count; i += 2)
            {
                if (items[i].GetType().Name == "ComboBox")
                {
                    if ((items[i] as ComboBox).SelectedIndex == -1)
                        return false;
                }
                else if (items[i].GetType().Name == "DateTimePicker")
                {
                    if (!(items[i] as DateTimePicker).Value.HasValue)
                        return false;
                }
                else if (items[i].GetType().Name == "TextBox")
                {
                    if ((items[i] as TextBox).Text == "")
                        return false;
                }
                else if (items[i].GetType().Name == "Grid")
                {
                    Grid grid = items[i] as Grid;
                    if ((grid.Children[0] as TextBox).Text == "" || (grid.Children[1] as TextBox).Text == "")
                        return false;
                }
                else
                {
                    throw new NotImplementedException("Type not implemented: " + items[i].GetType().Name);
                }
            }
            return true;
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate())
            {
                MessageBox.Show("Invalid data detected! Check all the fields before submitting");
                return;
            }
            ItemCollection items = itemsControl.Items;
            List<object> objectList = new List<object>();
            for (int i = 1; i < items.Count; i += 2)
            {
                if (items[i].GetType().Name == "ComboBox")
                {
                    objectList.Add((items[i] as ComboBox).SelectedItem);
                }
                else if (items[i].GetType().Name == "DateTimePicker")
                {
                    objectList.Add((items[i] as DateTimePicker).Value);
                }
                else if (items[i].GetType().Name == "TextBox")
                {
                    TextBox textBox = items[i] as TextBox;
                    if (textBox.Name == "Int")
                        objectList.Add(Int32.Parse(textBox.Text));
                    else
                        objectList.Add(textBox.Text);
                }
                else if (items[i].GetType().Name == "Grid")
                {
                    Grid grid = items[i] as Grid;
                    Tuple<double, double> tuple =
                        new Tuple<double, double>(
                            Double.Parse((grid.Children[0] as TextBox).Text, System.Globalization.CultureInfo.InvariantCulture),
                            Double.Parse((grid.Children[1] as TextBox).Text, System.Globalization.CultureInfo.InvariantCulture)
                        );
                    objectList.Add(tuple);
                }
                else
                {
                    throw new NotImplementedException("Type not implemented: " + items[i].GetType().Name);
                }
            }
            object[] objectArray = objectList.ToArray();
            IModel model = Activator.CreateInstance(fieldType, objectArray) as IModel;
            if (formMode == FormMode.Edit)
                Database.Edit(original, model);
            else if (formMode == FormMode.Add)
                Database.Add(model);
            window.Close();
        }

        public void Init(IModel model)
        {
            original = model;
            int validFields = CreateForm();
            window = new Window()
            {
                Width = 300,
                Height = (validFields * 66) + 50,
                Title = "Edit Form",
                ResizeMode = ResizeMode.NoResize,
            };
            window.Content = this;
            window.ShowDialog();

        }

        public void Init(Type type)
        {
            fieldType = type;
            int validFields = CreateForm();
            window = new Window()
            {
                Width = 300,
                Height = (validFields * 66) + 50,
                Title = formMode == FormMode.Edit ? "Edit Form" : "Add Form",
                ResizeMode = ResizeMode.NoResize,
            };
            window.Content = this;
            window.ShowDialog();
        }

        private int CreateForm()
        {
            if (formMode == FormMode.Edit)
                fieldType = original.GetType();
            PropertyInfo[] props = fieldType.GetProperties();
            int validFields = 1;
            foreach (var prop in props)
            {
                if (!Database.ExceptionTable.Contains(prop.Name))
                {
                    Label label = new Label()
                    {
                        Content = prop.Name,
                        BorderThickness = new Thickness()
                        {
                            Right = 5,
                            Bottom = 5,
                            Left = 5,
                            Top = 5,
                        },
                    };
                    itemsControl.Items.Add(label);
                    ++validFields;
                    if (prop.PropertyType.Namespace == "ReSys.Model")
                    {
                        ComboBox comboBox = new ComboBox()
                        {
                            ItemsSource = Database.GetListByTypeName(prop.PropertyType.Name),
                            Margin = new Thickness() { Right = 5, Bottom = 0, Left = 5, Top = 0, },
                        };
                        itemsControl.Items.Add(comboBox);
                    }
                    else if (prop.PropertyType.Name == "DateTime")
                    {
                        DateTimePicker picker = new DateTimePicker()
                        {
                            Margin = new Thickness() { Right = 5, Bottom = 0, Left = 5, Top = 0, },
                        };
                        itemsControl.Items.Add(picker);
                    }
                    else if (prop.PropertyType.Name == "String" || prop.PropertyType.Name == "string")
                    {
                        TextBox textBox = new TextBox()
                        {
                            Height = 36,
                            FontSize = 12,
                            Margin = new Thickness() { Right = 5, Bottom = 0, Left = 5, Top = 0, },
                        };
                        if (formMode == FormMode.Edit)
                        {
                            var propValue = prop.GetValue(original);
                            if (propValue != null)
                                textBox.Text = propValue.ToString();
                        }
                        itemsControl.Items.Add(textBox);
                    }
                    else if (prop.PropertyType.Name == "Tuple`2")
                    {
                        Tuple<double, double> pos = new Tuple<double, double>(0, 0);
                        if (formMode == FormMode.Edit)
                            pos = prop.GetValue(original) as Tuple<double, double>;
                        TextBox[] textBoxes = new TextBox[2];
                        Grid grid = new Grid();
                        ColumnDefinition[] columns = new ColumnDefinition[2];
                        for (int i = 0; i < 2; ++i)
                        {
                            columns[i] = new ColumnDefinition();
                            grid.ColumnDefinitions.Add(columns[i]);
                            string text;
                            if (formMode == FormMode.Edit)
                                text = (i == 0 ? pos.Item1.ToString() : pos.Item2.ToString());
                            else
                                text = "";
                            textBoxes[i] = new TextBox()
                            {
                                Height = 18,
                                FontSize = 12,
                                Margin = new Thickness() { Right = 5, Bottom = 0, Left = 5, Top = 0, },
                                Text = text,
                            };
                            textBoxes[i].PreviewTextInput += DecimalTextBox_PreviewTextInput;
                            textBoxes[i].PreviewKeyDown += DisallowSpaceTextBox_PreviewKeyDown;
                            Grid.SetColumn(textBoxes[i], i);
                            grid.Children.Add(textBoxes[i]);
                        }
                        itemsControl.Items.Add(grid);
                    }
                    else if (prop.PropertyType.Name == "Int32")
                    {
                        TextBox textBox = new TextBox()
                        {
                            Height = 18,
                            FontSize = 12,
                            Margin = new Thickness() { Right = 5,  Bottom = 0, Left = 5, Top = 0, },
                            Name = "Int",
                        };
                        textBox.PreviewTextInput += IntegerTextBox_PreviewTextInput;
                        textBox.PreviewKeyDown += DisallowSpaceTextBox_PreviewKeyDown;
                        if (formMode == FormMode.Edit)
                        {
                            var propValue = prop.GetValue(original);
                            if (propValue != null)
                                textBox.Text = propValue.ToString();
                        }
                        itemsControl.Items.Add(textBox);
                    }
                    else
                    {
                        throw new NotImplementedException("Type not implemented: " + prop.PropertyType.Name);
                    }
                }
            }
            return validFields;
        }

        private void DisallowSpaceTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)            
                e.Handled = true;            
        }

        private void IntegerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"^\d{0,9}$");
            if (!regex.IsMatch(((TextBox)sender).Text + e.Text))
                e.Handled = true;
        }

        private void DecimalTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        { 
            Regex regex = new Regex(@"^([\+-]?\d{0,3}(\.\d*)?)$");
            if(!regex.IsMatch(((TextBox)sender).Text + e.Text))
                e.Handled = true;            
        }
    }
}
