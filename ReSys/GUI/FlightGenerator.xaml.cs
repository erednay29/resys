﻿using ReSys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReSys
{
    public partial class FlightGenerator : UserControl
    {
        private Window window;

        public FlightGenerator()
        {
            InitializeComponent();
            routes.ItemsSource = Database.Routes;

            routes.SelectionChanged += Routes_SelectionChanged;
            startDate.ValueChanged += StartDate_ValueChanged;
            airplanes.SelectionChanged += Airplanes_SelectionChanged;
            buttonGenerate.Click += ButtonGenerate_Click;
            daily.Click += Daily_Click;
            weekly.Click += Weekly_Click;
        }


        public void Init()
        {
            window = new Window()
            {
                Width = 300,
                Height = 350,
                Title = "Generate flight",
                ResizeMode = ResizeMode.NoResize,
            };
            window.Content = this;
            window.ShowDialog();
        }

        private void Weekly_Click(object sender, RoutedEventArgs e)
        {
            if (weekly.IsChecked ?? false)
                daily.IsEnabled = false;
            else
                daily.IsEnabled = true;
        }

        private void Daily_Click(object sender, RoutedEventArgs e)
        {
            if (daily.IsChecked ?? false)
                weekly.IsEnabled = false;
            else
                weekly.IsEnabled = true;
        }

        private void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            Route route = routes.SelectedItem as Route;
            DateTime start = (DateTime)startDate.Value;
            Airplane airplane = airplanes.SelectedItem as Airplane;            
            Database.Add(new Flight(start, route, airplane, daily.IsChecked ?? false, weekly.IsChecked ?? false));
            window.Close();
        }

        private void Airplanes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (airplanes.SelectedIndex != -1)
                buttonGenerate.IsEnabled = true;
            else
                buttonGenerate.IsEnabled = false;
        }

        private void StartDate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Route route = (routes.SelectedItem as Route);
            double speed = 925.37;
            double time = route.Distance / speed;
            DateTime start = (DateTime)startDate.Value;
            DateTime end = start.AddHours(time);
            endDate.Text = end.ToString();
            IEnumerable<Flight> flights = Database.Flights.Where(x => x.StartDate < end && x.EndDate > startDate.Value);
            IEnumerable<Airplane> airplanesWithDistance = Database.Airplanes.Where(x => x.Range >= route.Distance);
            List<Airplane> correctAirplanes = new List<Airplane>();
            foreach (Airplane airplane in airplanesWithDistance)
            {
                if (!flights.Any(x => x.Airplane == airplane))
                    correctAirplanes.Add(airplane);
            }
            airplanes.ItemsSource = correctAirplanes;
            airplanes.IsEnabled = true;
        }

        private void Routes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (routes.SelectedIndex != -1)
            {
                startDate.IsEnabled = true;
                if (startDate.Value.HasValue)
                    StartDate_ValueChanged(sender, new RoutedPropertyChangedEventArgs<object>(null, null));
            }
            else
                startDate.IsEnabled = false;
        }
    }
}
