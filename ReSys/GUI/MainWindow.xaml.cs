﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using ReSys.Model;

namespace ReSys
{
    public partial class MainWindow : Window
    {
        private readonly string[] selectionBlackList = { "Flights", "Tickets" };

        private ListBoxItem selectedCategory;
        private string selectedCategoryName;
        private IList currentList;
        private DataGrid dataGrid = new DataGrid();
        private MainMenu menu = new MainMenu();

        public MainWindow()
        {
            InitializeComponent();
            AddEventHandlers();
            Grid.SetRow(dataGrid, 0);
            Grid.SetRow(menu, 0);
            display.Children.Add(menu);
            dataGrid.IsReadOnly = true;
        }

        private void AddEventHandlers()
        {
            select.SelectionChanged += Select_SelectionChanged;
            add.Click += Add_Click;
            remove.Click += Remove_Click;
            edit.Click += Edit_Click;
            dataGrid.SelectedCellsChanged += Display_SelectedCellsChanged;
            dataGrid.PreviewKeyDown += DataGrid_PreviewKeyDown;            
        }

        private void DataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                Database.Remove(dataGrid.SelectedItem as IModel);
                e.Handled = true;
                RefreshList();
            }
        }

        private void Display_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if ((sender as DataGrid).Items.Count != 0)
                remove.IsEnabled = true;
            if (!selectionBlackList.Contains(selectedCategoryName))
                edit.IsEnabled = true;
        }

        private void RefreshList()
        {
            currentList = typeof(Database).GetProperty(selectedCategoryName).GetValue(null) as IList;
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = currentList;
        }

        private void Select_SelectionChanged(object sender, EventArgs e)
        {
            selectedCategory = select.SelectedItem as ListBoxItem;
            selectedCategoryName = selectedCategory.Content.ToString();
            if (selectedCategoryName == "Main menu")
            {
                if (!display.Children.Contains(menu))
                {
                    if (display.Children.Contains(dataGrid))
                        display.Children.Remove(dataGrid);
                    display.Children.Add(menu);
                }
                remove.IsEnabled = false;
                edit.IsEnabled = false;
                add.IsEnabled = false;
            }
            else
            {
                if (!display.Children.Contains(dataGrid))
                    display.Children.Add(dataGrid);
                if (display.Children.Contains(menu))
                    display.Children.Remove(menu);
                if (selectedCategoryName.Contains(" "))
                {
                    string[] split = selectedCategoryName.Split(" ");
                    selectedCategoryName = "";
                    foreach (var item in split)
                    {
                        selectedCategoryName += char.ToUpper(item.First()) + item.Substring(1);
                    }
                }
                remove.IsEnabled = false;
                edit.IsEnabled = false;
                add.IsEnabled = false;
                if (!selectionBlackList.Contains(selectedCategoryName))
                    add.IsEnabled = true;
                RefreshList();
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            Type type = currentList.GetType().GetProperty("Item").PropertyType;
            FormControl editControl = new FormControl(FormMode.Add);
            editControl.Init(type);
            RefreshList();
            remove.IsEnabled = false;
            edit.IsEnabled = false;
        }

        private void Remove_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedIndex > -1)
            {
                Database.Remove(dataGrid.SelectedItem as IModel);
                RefreshList();
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedIndex > -1)
            {
                FormControl editControl = new FormControl(FormMode.Edit);
                editControl.Init(dataGrid.SelectedItem as IModel);
                RefreshList();
            }
        }

    }
}
