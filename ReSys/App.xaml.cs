﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace ReSys
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Database.Init();
            Database.ReadFromFile("master.txt");
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            Database.WriteToFile();
        }
    }
}
