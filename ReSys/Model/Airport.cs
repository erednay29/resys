﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Linq;

namespace ReSys.Model
{
    class Airport : IModel
    {
        public int ID { get; set; }
        public string Name { get; private set; }
        public Tuple<double, double> Position { get; private set; }

        public Airport()
        {
            Position = new Tuple<double, double>(0, 0);
        }

        public Airport(string name, Tuple<double, double> position)
        {
            Name = name;
            Position = position;           
        }

        public Airport(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Name = split[1];
            string[] pos = split[2].Split(",");
            Position = new Tuple<double, double>(Double.Parse(pos[0], new CultureInfo("en")),
                Double.Parse(pos[1], new CultureInfo("en")));
        }

        public string ToDatabaseFormat()
        {
            return ID + ";" + Name + ";"
                + Position.Item1.ToString(System.Globalization.CultureInfo.InvariantCulture)
                + ", " + Position.Item2.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
