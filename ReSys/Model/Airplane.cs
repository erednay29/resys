﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReSys.Model
{
    class Airplane : IModel
    {
        public int ID { get; set; }
        public int Range { get; private set; }
        public int Capacity { get; private set; }

        public Airplane()
        {
            Range = 0;
            Capacity = 0;
        }

        public Airplane(int range, int capacity)
        {
            Range = range;
            Capacity = capacity;
        }

        public Airplane(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Range = Int32.Parse(split[1]);
            Capacity = Int32.Parse(split[2]);
        }

        public string ToDatabaseFormat()
        {
            return ID + ";" + Range + ";" + Capacity;
        }

        public override string ToString()
        {
            return ID.ToString();
        }

    }
}
