﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Xaml;

namespace ReSys.Model
{
    class Ticket : IModel
    {
        public int ID { get; set; }
        public Flight Flight { get; private set; }
        public Client Client { get; private set; }

        public Ticket()
        {
            Flight = null;
        }

        public Ticket(Flight flight, Client client)
        {
            Flight = Database.Flights.ToImmutableList().Find(x => x == flight);
            Client = Database.Clients.ToImmutableList().Find(x => x == client);
        }

        public Ticket(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Flight = Database.Flights.ToImmutableList().Find(x => x.ID == Int32.Parse(split[1]));
            Client = Database.Clients.ToImmutableList().Find(x => x.ID == Int32.Parse(split[2]));
        }

        public string ToDatabaseFormat()
        {
            return ID + ";" + Flight.ID + ";" + Client.ID;
        }

        public override string ToString()
        {
            return Flight.ToString() + " | " + Client.ToString();
        }

    }
}
