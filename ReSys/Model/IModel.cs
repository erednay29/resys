﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReSys.Model
{
    public interface IModel
    {
        int ID { get; set; }
        public virtual string ToDatabaseFormat() { return null; }
       
    }
}
