﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Timers;

namespace ReSys.Model
{

    class Client : IModel
    {
        public int ID { get; set; }
        protected Client()
        {
            ID = 0;
        }

        public virtual string ToDatabaseFormat()
        {
            return ID.ToString();
        }

        public override string ToString()
        {
            return ID.ToString();
        }

    }

    class IndividualClient : Client
    {
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }

        public IndividualClient()
            : base()
        {
            Firstname = null;
            Lastname = null;
        }

        public IndividualClient(string firstname, string lastname)
            : base()
        {
            Firstname = firstname;
            Lastname = lastname;
        }

        public IndividualClient(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Firstname = split[1];
            Lastname = split[2];
        }

        public override string ToDatabaseFormat()
        {
            return ID + ";" + Firstname + ";" + Lastname;
        }

        public override string ToString()
        {
            return Firstname + " " + Lastname;
        }

    }

    class CompanyClient : Client
    {
        public string Name { get; private set; }

        public CompanyClient()
            : base()
        {
            Name = null;
        }

        public CompanyClient(string name)
            : base()
        {
            Name = name;
        }

        public CompanyClient(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Name = split[1];
        }

        public override string ToDatabaseFormat()
        {
            return ID + ";" + Name;
        }

        public override string ToString()
        {
            return Name;
        }

    }

}
