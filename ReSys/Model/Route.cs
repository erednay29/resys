﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ReSys.Model
{
    class Route : IModel
    {
        public int ID { get; set; }
        public Airport Source { get; private set; }
        public Airport Destination { get; private set; }
        public double Distance { get; private set; }

        private void CalculateDistance()
        {            
            double R = 6378.137; // Radius of earth in KM
            double dLat = Destination.Position.Item1 * Math.PI / 180 - Source.Position.Item1 * Math.PI / 180;
            double dLon = Destination.Position.Item2 * Math.PI / 180 - Source.Position.Item2 * Math.PI / 180;
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            Math.Cos(Source.Position.Item1 * Math.PI / 180) * Math.Cos(Destination.Position.Item1 * Math.PI / 180) *
            Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            Distance = R * c;
        }

        public Route()
        {
            Source = null;
            Destination = null;
            Distance = 0;
        }

        public Route(Airport src, Airport dest)
        {
            Source = src;
            Destination = dest;
            CalculateDistance();        
        }

        public Route(string[] split)
        {
            ID = Int32.Parse(split[0]);            
            Source = Database.Airports.ToImmutableList().Find(x => x.ID == Int32.Parse(split[1]));
            Destination = Database.Airports.ToImmutableList().Find(x => x.ID == Int32.Parse(split[2]));            
            Distance = Double.Parse(split[3], System.Globalization.CultureInfo.InvariantCulture);
        }

        public string ToDatabaseFormat()
        {
            return ID + ";" + Source.ID + ";" + Destination.ID + ";" + Distance.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public override string ToString()
        {
            return Source.ToString() + "->" + Destination.ToString();
        }

    }
}
