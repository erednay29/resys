﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ReSys.Model
{
    class Flight : IModel
    {
        public int ID { get; set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public Route Route { get; private set; }
        public Airplane Airplane { get; private set; }
        public bool Daily { get; private set; }
        public bool Weekly { get; private set; }

        public Flight()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            Route = null;
            Airplane = null;
        }

        public Flight(DateTime startDate, Route route, Airplane airplane)
        {
            double speed = 925.37;
            double time = route.Distance / speed;
            Route = Database.Routes.ToImmutableList().Find(x => x == route);
            StartDate = startDate;
            EndDate = startDate.AddHours(time);
            Airplane = airplane;
            Daily = false;
            Weekly = false;
        }

        public Flight(DateTime startDate, Route route, Airplane airplane, bool daily, bool weekly)
        {
            double speed = 925.37;
            double time = route.Distance / speed;
            Route = Database.Routes.ToImmutableList().Find(x => x == route);
            StartDate = startDate;
            EndDate = startDate.AddHours(time);
            Airplane = airplane;
            Daily = daily;
            Weekly = weekly;
        }

        public Flight(string[] split)
        {
            ID = Int32.Parse(split[0]);
            Route = Database.Routes.ToImmutableList().Find(x => x.ID == Int32.Parse(split[1]));
            StartDate = DateTime.ParseExact(split[2], "yyyyMMddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture);
            EndDate = DateTime.ParseExact(split[3], "yyyyMMddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture);
            Airplane = Database.Airplanes.ToImmutableList().Find(x => x.ID == Int32.Parse(split[4]));
            Daily = Convert.ToBoolean(Int32.Parse(split[5]));
            Weekly = Convert.ToBoolean(Int32.Parse(split[6]));
        }

        public string ToDatabaseFormat()
        {
            return ID + ";" + Route.ID + ";"
                + StartDate.ToString("yyyyMMddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture) + ";"
                + EndDate.ToString("yyyyMMddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture) + ";"
                + Airplane.ID + ";" + Convert.ToInt32(Daily) + ";" + Convert.ToInt32(Weekly);
        }

        public override string ToString()
        {
            return Route.ToString() + " | Start date: " + StartDate.ToString();
        }

    }
}
